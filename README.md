<!--
Copyright 2021 Pi Fisher (3geek14)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Realms #

This repository maintains the Realms Omnibus, and is primarily used by the
Omnibus Editorial Committee. If you happen to prefer git pull requests over the
proposal system on the Realms website, Pi is willing to convert pull requests
from here into proposals on [RealmsNet](https://www.realmsnet.net/). Please do
not assume your pull requests will be merged if you aren't on the OEC; they will
most likely be closed.

# Guides #

This version of the Omnibus is written in Markdown. Please read the [Markdown
tutorial](https://3geek14.gitlab.io/Realms/MarkdownTutorial) to learn how to use
Markdown. If you already know Markdown, please read the tutorial so that the
code is consistent.

To convert from Markdown to HTML, we use Pandoc. [Pandoc's
website](http://www.pandoc.org/installing.html) has installation instructions.
Please use version 2.7, so that you have the same version as everyone else. Once
installed, you can compile a new version of the Omnibus with the command:

    pandoc -o Omnibus.html -s -H internal.css -c external.css Omnibus.md

This project is maintained with git on GitLab. There are many [GitLab
tutorials](https://docs.gitlab.com/ee/gitlab-basics/) available. Please read
one of those, and then go over our [editing
procedures](https://3geek14.gitlab.io/Realms/EditingProcedures) document.